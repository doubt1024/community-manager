package com.wjxy.communityapp.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wjxy.communityapp.dto.AddressDto;
import com.wjxy.communityapp.dto.UserDto;
import com.wjxy.communityapp.dto.UsersaltDto;
import com.wjxy.communityapp.entity.UsersaltEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wjxy.communityapp.utils.Result;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Fluency
 * @since 2021-03-05
 */
public interface UsersaltMapper extends BaseMapper<UsersaltDto> {

}
