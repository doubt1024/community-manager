package com.wjxy.communityapp.mapper;

import com.wjxy.communityapp.dto.MenuDTO;
import com.wjxy.communityapp.entity.MenuEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Fluency
 * @since 2021-03-30
 */
public interface MenuMapper extends BaseMapper<MenuEntity> {

    List<MenuDTO> listByUserId(Integer userId);

}
