package com.wjxy.communityapp.mapper;

import com.wjxy.communityapp.dto.VercodeDto;
import com.wjxy.communityapp.entity.VercodeEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Fluency
 * @since 2021-03-08
 */
public interface VercodeMapper extends BaseMapper<VercodeDto> {

}
