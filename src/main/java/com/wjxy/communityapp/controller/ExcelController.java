package com.wjxy.communityapp.controller;

import com.alibaba.excel.EasyExcel;
import com.wjxy.communityapp.common.AddExcelListener;
import com.wjxy.communityapp.common.ExcelListener;
import com.wjxy.communityapp.dto.AddressDto;
import com.wjxy.communityapp.dto.ResidentDto;
import com.wjxy.communityapp.service.AddressService;
import com.wjxy.communityapp.service.ResidentService;
import com.wjxy.communityapp.utils.Result;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;
import java.io.InputStream;

/**
 * @desc: excel导入数据库
 * @author: LiuCh
 * @since: 2021/5/6
 */
@Api(value = "文件上传", tags = "文件上传相关接口")
@RestController
@RequestMapping("/api/excel")
public class ExcelController {
    public static Logger logger = LoggerFactory.getLogger(ExcelController.class);
    @Resource
    private ResidentService residentService;
    @Resource
    private AddressService addressService;

    @PostMapping("/uploadRes")
    public Result upload(@RequestParam MultipartFile file) {
        InputStream fileInputStream = null;
        try {
            fileInputStream = file.getInputStream();
        } catch (IOException e) {
            e.printStackTrace();
            return Result.fail("上传文件异常");
        }
        try {
            EasyExcel.read(fileInputStream, ResidentDto.class, new ExcelListener(residentService)).sheet().doRead();
            return Result.ok("上传文件成功");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Result.fail("未知错误");
    }

    @PostMapping("/uploadAdd")
    public Result uploadd(@RequestParam MultipartFile file) {
        InputStream fileInputStream = null;
        try {
            fileInputStream = file.getInputStream();
        } catch (IOException e) {
            e.printStackTrace();
            return Result.fail("上传文件异常");
        }
        try {
            EasyExcel.read(fileInputStream, AddressDto.class, new AddExcelListener(addressService)).sheet().doRead();
            return Result.ok("上传文件成功");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Result.fail("未知错误");
    }
}
