package com.wjxy.communityapp.utils;

import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.util.ByteSource;

/**
 * @author Fluency
 * @creat 2021-01
 */
public class MD5Util {

    public static String setSalt(String account, String pwd)
    {
        String hashAlgorithName = "MD5";//加密算法
        String password = pwd;//登陆时的密码
        int hashIterations =1024;//加密次数
        ByteSource credentialsSalt = ByteSource.Util.bytes(account);//使用登录名做为salt
        SimpleHash simpleHash = new SimpleHash(hashAlgorithName, password, credentialsSalt, hashIterations);
        return simpleHash.toHex();
    }

}
