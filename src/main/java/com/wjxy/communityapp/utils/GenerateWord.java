package com.wjxy.communityapp.utils;

import sun.java2d.pipe.AAShapePipe;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @author Fluency
 * @creat 2021-01
 *
 * 生成随机数工具类( 废弃， 现在使用commons类库)
 */
public class GenerateWord {
    //生成4位纯数字随机数
    public String RandomNum() {
        String[] beforeShuffle = new String[] { "0","1","2", "3", "4", "5", "6", "7",
                "8", "9" };
        List list = Arrays.asList(beforeShuffle);
        Collections.shuffle(list);
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < list.size(); i++) {
            sb.append(list.get(i));
        }
        String afterShuffle = sb.toString();
        String result = afterShuffle.substring(5, 9);
        return result;
    }

    //生成加密字符串
    public String RandomString(){

        String result = "";
        return result;
    }
}
