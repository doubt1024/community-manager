package com.wjxy.communityapp.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.wjxy.communityapp.dto.RecordDto;
import com.wjxy.communityapp.dto.ResidentDto;
import com.wjxy.communityapp.entity.RecordEntity;
import com.baomidou.mybatisplus.extension.service.IService;
import com.wjxy.communityapp.utils.Result;
import org.apache.ibatis.annotations.Param;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Fluency
 * @since 2021-01-11
 */
public interface RecordService extends IService<RecordEntity> {

    IPage<RecordDto> queryRecordByParam(Integer currentPage, Integer limit, String keyword);

    Result addRecord(RecordDto recordDto);

    //根据ID查询住户信息
    RecordDto queryRecordById(String recId);

    //    修改操作
    Result updateRecord(RecordDto recordDto);
        //    删除数据通过ResID（一条或多条）实际是更新操作
    Result deleteRecord(Integer[] ids);

    IPage<RecordDto> queryRecordByParam();

    List<RecordDto> queryRecCountByDate(String beginTime, String endTime);

}
