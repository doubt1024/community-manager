package com.wjxy.communityapp.service.impl;

import com.wjxy.communityapp.dto.RoleAuthDto;
import com.wjxy.communityapp.entity.RoleAuthEntity;
import com.wjxy.communityapp.mapper.RoleAuthMapper;
import com.wjxy.communityapp.service.RoleAuthService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Fluency
 * @since 2021-01-08
 */
@Service
public class RoleAuthServiceImpl extends ServiceImpl<RoleAuthMapper, RoleAuthEntity> implements RoleAuthService {

    @Autowired
    private RoleAuthMapper roleAuthMapper;
    @Override
    public List<RoleAuthDto> getAuthByRole(int roleId) {
        List<RoleAuthDto> authByRole = roleAuthMapper.getAuthByRole(roleId);
        return authByRole;
    }
}
