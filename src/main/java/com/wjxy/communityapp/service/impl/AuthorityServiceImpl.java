package com.wjxy.communityapp.service.impl;

import com.wjxy.communityapp.entity.AuthorityEntity;
import com.wjxy.communityapp.mapper.AuthorityMapper;
import com.wjxy.communityapp.service.AuthorityService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Fluency
 * @since 2021-01-08
 */
@Service
public class AuthorityServiceImpl extends ServiceImpl<AuthorityMapper, AuthorityEntity> implements AuthorityService {

}
