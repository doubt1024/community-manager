package com.wjxy.communityapp.service.impl;

import com.wjxy.communityapp.entity.UserRoleEntity;
import com.wjxy.communityapp.mapper.UserRoleMapper;
import com.wjxy.communityapp.service.UserRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户角色表 服务实现类
 * </p>
 *
 * @author Fluency
 * @since 2021-01-27
 */
@Service
public class UserRoleServiceImpl extends ServiceImpl<UserRoleMapper, UserRoleEntity> implements UserRoleService {

}
