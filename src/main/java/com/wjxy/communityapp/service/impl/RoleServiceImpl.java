package com.wjxy.communityapp.service.impl;

import com.wjxy.communityapp.entity.RoleEntity;
import com.wjxy.communityapp.mapper.RoleMapper;
import com.wjxy.communityapp.service.RoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Fluency
 * @since 2021-01-08
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, RoleEntity> implements RoleService {

    @Autowired
    private RoleMapper roleMapper;


    @Override
    public String findRoleByAccount(String loginAccount) {
        String role = roleMapper.findRoleByAccount(loginAccount);
        return role;
    }
}
