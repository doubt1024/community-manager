package com.wjxy.communityapp.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.wjxy.communityapp.dto.RecordDto;
import com.wjxy.communityapp.dto.VistorDto;
import com.wjxy.communityapp.entity.VisitorEntity;
import com.baomidou.mybatisplus.extension.service.IService;
import com.wjxy.communityapp.utils.Result;

/**
 * <p>
 * 来访客人登记表 服务类
 * </p>
 *
 * @author Fluency
 * @since 2021-01-11
 */
public interface VisitorService extends IService<VisitorEntity> {

    IPage<VistorDto> queryVistorByParam(Integer currentPage, Integer limit, String keyword);

    Result addVistor(VistorDto vistorDto);

    Result deleteVistors(Integer[] ids);

    Result updateVistor(VistorDto vistorDto);

    IPage<VistorDto> queryVistorByParam();

}
