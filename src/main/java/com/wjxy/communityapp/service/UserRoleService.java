package com.wjxy.communityapp.service;

import com.wjxy.communityapp.entity.UserRoleEntity;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户角色表 服务类
 * </p>
 *
 * @author Fluency
 * @since 2021-01-27
 */
public interface UserRoleService extends IService<UserRoleEntity> {

}
