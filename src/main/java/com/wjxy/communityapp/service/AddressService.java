package com.wjxy.communityapp.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.wjxy.communityapp.dto.AddressDto;
import com.wjxy.communityapp.entity.AddressEntity;
import com.wjxy.communityapp.utils.Result;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author Fluency
 * @since 2021-01-19
 */
public interface AddressService extends IService<AddressEntity> {

    IPage<AddressDto> queryAddressByParam(Integer currentPage, Integer limit, String keyword);

    Result addAddress(AddressDto addressDto);

    Result updateAddress(AddressDto addressDto);

    Result deleteAddress(Integer[] ids);

    IPage<AddressDto> queryAddressByParam();

    int insertBatchAddress(List<AddressDto> addressList);
}
