package com.wjxy.communityapp.service;

import com.wjxy.communityapp.entity.AuthorityEntity;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Fluency
 * @since 2021-01-08
 */
public interface AuthorityService extends IService<AuthorityEntity> {

}
